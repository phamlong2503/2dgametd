﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Monster target;

    private Tower tower;

    private Animator myAnimator;

    private Element element;

    void Awake()
    {
        this.myAnimator = GetComponent<Animator>();
    }

    public void Initialize(Tower tower)
    {
        this.target = tower.Target;
        this.tower = tower;
        this.element = tower.ElementType;
    }

    void Update()
    {
        if (target != null && target.IsActive)          
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * tower.ProjectileSpeed);

            Vector2 dir = target.transform.position - transform.position;

            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        else if(!target.IsActive)            
        {
            GameManager.Instance.Pool.ReleaseObject(gameObject);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Monster")
        {
            Monster target = other.GetComponent<Monster>();

            target.TakeDamage(tower.Damage, tower.ElementType);

            myAnimator.SetTrigger("Impact");

            ApplyDebuff();
        }

    }

    private void ApplyDebuff()
    {
        if (target.ElementType != element)
        {
            float roll = UnityEngine.Random.Range(0, 100);

            if (roll <= tower.Proc)
            {   
                target.AddDebuff(tower.GetDebuff());
            }
        }

    }
}
