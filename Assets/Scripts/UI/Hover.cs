﻿using UnityEngine;

public class Hover : Singleton<Hover>
{
    private SpriteRenderer spriteRenderer;

    private SpriteRenderer rangeSpriteRenderer;

    public bool Visible { get; set; }

    void Awake()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.rangeSpriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }
	
	void Update ()
    {
        if (spriteRenderer.enabled) 
        {
            FollowMouse();
        }
   
	}

    private void FollowMouse()
    {
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void Activate(Sprite sprite)
    {
        Visible = true;
        spriteRenderer.enabled = true;
        rangeSpriteRenderer.enabled = true;
        spriteRenderer.sprite = sprite;
    }

    public void Deactivate()
    {
        Visible = false;
        rangeSpriteRenderer.enabled = false;
        spriteRenderer.enabled = false;
    }
}
