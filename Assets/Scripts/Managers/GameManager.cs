﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Element { STORM, FIRE, FROST, POISON, NONE }

public delegate void CurrencyChanged();

public class GameManager : Singleton<GameManager>
{
    
    #region EVENTS

    public event CurrencyChanged Changed;

    #endregion

    #region VARIABLES

    private Tower selectedTower;

    private int currency;

    private int lives;

    private int wave = 0;

    private int health = 10;

    private bool gameOver;

    private List<Monster> activeMonsters = new List<Monster>();

    [SerializeField]
    private Sprite[] towerSprites;

    [SerializeField]
    private GameObject pauseMenu;

    [SerializeField]
    private GameObject upgradePanel;

    [SerializeField]
    private GameObject statsPanel;

    [SerializeField]
    private GameObject gameOverMenu;

    [SerializeField]
    private GameObject waveBtn;

    [SerializeField]
    private Text upgradePrice;

    [SerializeField]
    private Text livesText;

    [SerializeField]
    private Text waveText;

    [SerializeField]
    private Text currencyText;

    [SerializeField]
    private Text sellText;

    [SerializeField]
    private Text stats;

    [SerializeField]
    private Text sizeText;

    #endregion

    #region PROPERTIES

    public ObjectPool Pool { get; private set; }

    public TowerBtn ClickedBtn { get; set; }

    public int Lives
    {
        get
        {
            return lives;
        }
        set
        {

            this.lives = value;
           

            if (lives <= 0)
            {
                lives = 0;
                GameOver();
            }

            livesText.text = lives.ToString();
        }
    }

    public int Currency
    {
        get
        {
            return currency;
        }

        set
        {
            this.currency = value;

            this.currencyText.text = value.ToString() + " <color=lime>$</color>";

            OnCurrencyChanged();
        }
    }

    public Text Stats
    {
        get
        {
            return stats;
        }
    }

    public Text SizeText
    {
        get
        {
            return sizeText;
        }
    }

    #endregion

    private void Awake()
    {
        Pool = GetComponent<ObjectPool>();
    }

    private void Start()
    {
        Lives = 10;
        Currency = 1000;

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))     
        {
            if (selectedTower == null && !Hover.Instance.Visible)
            {
                MenuManager.Instance.ShowIngameMenu();
            }
            else if (Hover.Instance.Visible)            
            {
                DropTower();
            }
            else if (selectedTower != null)          
            {
                DeselectTower();
            }
        }
    }

    public void OnCurrencyChanged()
    {
        if (Changed != null)
        {
            Changed(); 
        }
    }

    public void PickTower(TowerBtn towerBtn)
    {
        if (Currency >= towerBtn.Price && activeMonsters.Count <= 0)
        {
            ClickedBtn = towerBtn; 

            Hover.Instance.Activate(ClickedBtn.TowerSprite);
        }
    }

    private void DropTower()
    {
        ClickedBtn = null;

        Hover.Instance.Deactivate();
    }

    public void BuyTower()
    {
        if (Currency >= ClickedBtn.Price)
        {
            Currency -= ClickedBtn.Price;

            ClickedBtn = null;
        }
    }

    public void SelectTower(Tower tower)
    {
        if (selectedTower != null)     
        {
            selectedTower.Select();
        }

        selectedTower = tower; 

        sellText.text = "+ " + (selectedTower.Price / 2).ToString() + " $";

        upgradePanel.SetActive(true);

        if (tower.NextUpgrade != null)
        {
            upgradePrice.text = tower.NextUpgrade.Price.ToString() + "$";
        }
      
        selectedTower.Select();
    }

    public void UpdateTooltip()
    {
        if (selectedTower != null)
        {
            sellText.text = "+ " + (selectedTower.Price / 2).ToString() + " $";
            SetTooltipText(selectedTower.GetStats());

            if (selectedTower.NextUpgrade != null)            
            {
                upgradePrice.text = selectedTower.NextUpgrade.Price.ToString() + "$";
            }
            else
            {
                upgradePrice.text = string.Empty;
            }
           
        }
    }

    public void SetTooltipText(string txt)
    {
        stats.text = txt;    
        sizeText.text = txt;    

    }

    public void DeselectTower()
    {
        if (selectedTower != null)
        {
            selectedTower.Select();
        }

        upgradePanel.SetActive(false);

        selectedTower = null;
    }

    public void SellTower()
    {
        if (selectedTower != null)
        {
            Currency += selectedTower.Price / 2;

            selectedTower.GetComponentInParent<TileScript>().IsEmpty = true;

            Destroy(selectedTower.transform.parent.gameObject);

            DeselectTower();

        }
    }

    public void UpgradeTower()
    {
        if (selectedTower != null)
        {
            if (selectedTower.Level <= selectedTower.Upgrades.Length && Currency >= selectedTower.NextUpgrade.Price)
            {
                selectedTower.Upgrade();
            }
               
        }
    }

    public void ShowStats()
    {
        statsPanel.SetActive(!statsPanel.activeSelf);
    }

    public void StartWave()
    {
        wave++;

        waveText.text = string.Format("Wave: <color=lime>{0}</color>",wave);

        StartCoroutine(SpawnWave());

        waveBtn.SetActive(false);
    }

    private IEnumerator SpawnWave()
    {
        LevelManager.Instance.GeneratePath();

        for (int i = 0; i < wave; i++)
        {
            int monsterIndex = UnityEngine.Random.Range(0, 4);

            string type = string.Empty;

            switch (monsterIndex)
            {
                case 0:
                    type = "BlueMonster";
                    break;
                case 1:
                    type = "RedMonster";
                    break;
                case 2:
                    type = "GreenMonster";
                    break;
                case 3:
                    type = "PurpleMonster";
                    break;
            }

            Monster monster = Pool.GetObject(type).GetComponent<Monster>();

            monster.Spawn(health);

            if (wave % 3 == 0)
            {
                health += 5;
            }

            activeMonsters.Add(monster);

            yield return new WaitForSeconds(2.5f);
        }
    }

    public void GameOver()
    {
        if (!gameOver)
        {
            gameOver = true;

            gameOverMenu.SetActive(true);
        }
        
    }

    public void Restart()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void RemoveMonster(Monster monster)
    {
        activeMonsters.Remove(monster);

        if (activeMonsters.Count <= 0 && !gameOver)
        {
            waveBtn.SetActive(true);
        }
    }
}