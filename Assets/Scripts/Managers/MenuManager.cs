﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : Singleton<MenuManager>
{
    [SerializeField]
    private GameObject mainMenu;

    [SerializeField]
    private GameObject optionMenu;

    public void ShowOptions()
    {
        mainMenu.SetActive(false);
        optionMenu.SetActive(true);
    }

    public void ShowMain()
    {
        optionMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitToMain()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene(0);
    }

    public void ShowIngameMenu()
    {
        if (optionMenu.activeSelf)
        {
            ShowMain();
        }
        else     
        {
            mainMenu.SetActive(!mainMenu.activeSelf);

            if (!mainMenu.activeSelf)     
            {
                Time.timeScale = 1;
            }
            else     
            {
                Time.timeScale = 0;
            }
        }

    }
}
