﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    private GameObject playBtn;

    [SerializeField]
    private Canvas loadScreen;

    [SerializeField]
    private GameObject[] tilePrefabs;

    [SerializeField]
    private GameObject bluePortalPrefab;

    [SerializeField]
    private GameObject redPortalPrefab;

    [SerializeField]
    private Transform map;

    [SerializeField]
    private CameraMovement cameraMovement;

    [SerializeField]
    private Stat loadingStat;

    public bool Loading { get; private set; }

    public Portal BluePortal { get; private set; }

    private Point mapSize;

    public Point RedSpawn { get; private set; }

    private Point blueSpawn;

    private Stack<Node> fullPath;

    public Dictionary<Point, TileScript> Tiles { get; private set; }

    public float TileSize
    {
        get { return tilePrefabs[0].GetComponent<SpriteRenderer>().sprite.bounds.size.x * tilePrefabs[0].GetComponent<Transform>().localScale.x; }
    }

    private Vector3 WorldStartPos
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height));
        }
    }

    public Stack<Node> Path
    {
        get
        {
            if (fullPath == null)
            {
                GeneratePath();
            }

            return new Stack<Node>(new Stack<Node>(fullPath));
        }
    }

    void Start()
    {
        loadingStat.Initialize();

        StartCoroutine(CreateLevel());
    }

    private IEnumerator CreateLevel()
    {
        string[] mapData;

        Loading = true;

        Tiles = new Dictionary<Point, TileScript>();

        mapData = ReadlLevelText();

        mapSize = new Point(mapData[0].ToCharArray().Length, mapData.Length);

        float maxTiles = mapSize.X * mapSize.Y;

        loadingStat.MaxVal = maxTiles;

        float actualTiles = 0;

        for (int y = 0; y < mapSize.Y; y++)
        {
            char[] horizontalTiles = mapData[y].ToCharArray();

            for (int x = 0; x < mapSize.X; x++)
            {
                PlaceTile(horizontalTiles[x].ToString(), x, y);
                actualTiles++;

                loadingStat.CurrentValue = actualTiles;

                yield return null;

            }
        }
        Vector3 maxtilePosition = Tiles[new Point(mapSize.X - 1, mapSize.Y - 1)].transform.position;

        cameraMovement.SetLimits(new Vector3(maxtilePosition.x + TileSize, maxtilePosition.y - TileSize));

        SpawnPortals();

        playBtn.SetActive(true);
    }


    private void SpawnPortals()
    {
        blueSpawn = new Point(0, 0);
        GameObject bluePortal = (GameObject)Instantiate(bluePortalPrefab, Tiles[blueSpawn].GetComponent<TileScript>().WorldPosition, Quaternion.identity);
        bluePortal.name = "BluePortal";
        this.BluePortal = bluePortal.GetComponent<Portal>();

        RedSpawn = new Point(11, 6);
        GameObject redPortal = (GameObject)Instantiate(redPortalPrefab, Tiles[RedSpawn].GetComponent<TileScript>().WorldPosition, Quaternion.identity);
        redPortal.name = "RedPortal";
    }

    public bool InBounds(Point position)
    {
        return position.X >= 0 && position.Y >= 0 && position.X < mapSize.X && position.Y < mapSize.Y;
    }

    private string[] ReadlLevelText()
    {
        TextAsset bindata = Resources.Load("Level") as TextAsset;

        string data = bindata.text.Replace(Environment.NewLine, string.Empty);

        return data.Split('-');
    }

    private void PlaceTile(string tileType, int x, int y)
    {
        int tileIndex = int.Parse(tileType);

        TileScript tile = Instantiate(tilePrefabs[tileIndex]).GetComponent<TileScript>();

        tile.Setup(new Point(x, y), new Vector3(WorldStartPos.x + (TileSize * x), WorldStartPos.y - (TileSize * y), 0), map);
    }

    public void GeneratePath()
    {
        fullPath = AStar.GetPath(blueSpawn, RedSpawn);
    }

    public void StartLevel()
    {
        loadScreen.enabled = false;

        Loading = false;
    }

    public bool CheckPath(Point checkPoint)
    {
        TileScript tmp = Tiles[checkPoint];

        if (tmp.IsEmpty)          
        {
            tmp.IsEmpty = false;

            if (AStar.GetPath(RedSpawn, blueSpawn) == null)         
            {
                tmp.IsEmpty = true; 
                return false;
            }
            else      
            {
                tmp.IsEmpty = true;
                return true;
            }

        }
        else     
        {
            return false;
        }

    }
}
