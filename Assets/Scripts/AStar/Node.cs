﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Node : IComparable<Node>
{
    public Point GridPosition { get; set; }

    public Vector2 WorldPosition { get; set; }

    public Node Parent { get; set; }

    public int G { get; set; }

    public int H { get; set; }

    public int F { get; set; }

    public Node(TileScript tileScript)
    {
        this.GridPosition = tileScript.GridPosition;
        this.WorldPosition = tileScript.WorldPosition;
    }

    public void CalcValues(Node parentNode, Node goalNode, int cost)
    {
        Parent = parentNode;

        G = parentNode.G + cost;

        H = ((Math.Abs((int)(GridPosition.X - goalNode.GridPosition.X)) + Math.Abs((int)(goalNode.GridPosition.Y - GridPosition.Y))) * 10);

        F = G + H;
    }

    public void Reset()
    {
        G = 0;
        F = 0;
        H = 0;
        Parent = null;
    }

    public int CompareTo(Node other)
    {
        if (F > other.F)               
        {
            return 1;
        }
        else if (F < other.F)              
        {
            return -1;
        }

        return 0;             
    }

}
