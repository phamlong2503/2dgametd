﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public static class AStar
{
    private static Dictionary<Point, Node> nodes;

    private static void CreateNodes()
    {
        nodes = new Dictionary<Point, Node>();

        foreach (TileScript tile in LevelManager.Instance.Tiles.Values)
        {
            nodes.Add(tile.GridPosition, new Node(tile));
        }
    }

    public static Stack<Node> GetPath(Point start, Point goal)
    {
        if (nodes == null)            
        {
            CreateNodes();
        }

        foreach (Node node in nodes.Values) 
        {
            node.Reset();
        }

        Node currentNode = nodes[start];

        HashSet<Node> openList = new HashSet<Node>();

        HashSet<Node> closedList = new HashSet<Node>();

        openList.Add(currentNode);

        while (openList.Count > 0)                  
        {
            for (int x = -1; x <= 1; x++)             
            {
                for (int y = -1; y <= 1; y++)
                {
                    Point neighbourPos = new Point(currentNode.GridPosition.X - x, currentNode.GridPosition.Y - y);

                    if (LevelManager.Instance.InBounds(neighbourPos) && neighbourPos != start && LevelManager.Instance.Tiles[neighbourPos].IsEmpty && neighbourPos != currentNode.GridPosition)
                    {
                        Node neighbour = nodes[new Point(neighbourPos.X, neighbourPos.Y)];

                        int gCost = 0;

                        if (Math.Abs(x - y) % 2 == 1)
                        {
                            gCost = 10;          
                        }
                        else      
                        {
                            if (!ConnectedDiagonally(currentNode, neighbour))
                            {
                                continue;
                            }

                            gCost = 14;        
                        }

                        if (openList.Contains(neighbour))      
                        {
                            if (currentNode.G + gCost < neighbour.G)            
                            {
                                neighbour.CalcValues(currentNode, nodes[goal], gCost);
                            }
                        }
                        else if (!closedList.Contains(neighbour))               
                        {
                            neighbour.CalcValues(currentNode, nodes[goal], gCost);       

                            if (!openList.Contains(neighbour))        
                            {
                                openList.Add(neighbour);          
                            }
                        }
                    }
                }
            }

            openList.Remove(currentNode);
              
            closedList.Add(currentNode);

            if (openList.Count > 0)                 
            {
                currentNode = openList.OrderBy(x => x.F).First();                   
            }

            if (currentNode == nodes[goal])            
            {
                Stack<Node> finalPath = new Stack<Node>();

                while (currentNode.GridPosition != start)
                {
                    finalPath.Push(currentNode);
                    currentNode = currentNode.Parent;
                }

                return finalPath;
            }
        }

        return null;

    }

    private static bool ConnectedDiagonally(Node currentNode, Node neighbour)
    {
        Point direction = currentNode.GridPosition - neighbour.GridPosition;

        Point first = new Point(currentNode.GridPosition.X + (direction.X * -1), currentNode.GridPosition.Y);
        Point second = new Point(currentNode.GridPosition.X, currentNode.GridPosition.Y + (direction.Y * -1));

        if (LevelManager.Instance.InBounds(first) && !LevelManager.Instance.Tiles[first].IsEmpty)
        {
            return false;
        }
        if (LevelManager.Instance.InBounds(second) && !LevelManager.Instance.Tiles[second].IsEmpty)
        {
            return false;
        }

        return true;
    }
}
