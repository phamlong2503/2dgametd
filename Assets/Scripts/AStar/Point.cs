﻿
public struct Point
{
    public int X { get; set; }

    public int Y { get; set; }

    public Point(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }

    public static bool operator ==(Point x, Point y)
    {
        return x.X == y.X && x.Y == y.Y;
    }

    public static bool operator !=(Point x, Point y)
    {
        return x.X != y.X || x.Y != y.Y;
    }

    public static Point operator -(Point x, Point y)
    {
        return new Point(x.X - y.X, x.Y - y.Y);
    }

    public static Point operator /(Point x, int value)
    {
        return new Point(x.X / value, x.Y / value);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        Point other = (Point)obj;
        return X == other.X && Y == other.Y;
    }

}
