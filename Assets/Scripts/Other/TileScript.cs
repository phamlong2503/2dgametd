﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TileScript : MonoBehaviour
{
    public Point GridPosition { get; private set; }

    public bool IsEmpty { get; set; }

    private Color32 fullColor = new Color32(255, 118, 118, 255);

    private Color32 emptyColor = new Color32(96, 255, 90, 255);

    private SpriteRenderer spriteRenderer;

    private Tower myTower;

    public Vector2 WorldPosition
    {
        get
        {
            return new Vector2(transform.position.x + (GetComponent<SpriteRenderer>().bounds.size.x/2), transform.position.y - (GetComponent<SpriteRenderer>().bounds.size.y / 2));
        }
    }

    public void Start()
    {
        IsEmpty = true;

        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Setup(Point gridPosition, Vector3 worldPosition, Transform parent)
    {
        this.GridPosition = gridPosition;
        transform.position = worldPosition;
        transform.SetParent(parent);

        LevelManager.Instance.Tiles.Add(GridPosition, this);
    }

    public void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedBtn != null)
        {
            if (IsEmpty)
            {
                ColorTile(emptyColor);    
            }
            if(!LevelManager.Instance.CheckPath(GridPosition) || GridPosition == LevelManager.Instance.RedSpawn)        
            {
                ColorTile(fullColor);
            }
            else if (Input.GetMouseButtonDown(0))   
            {
                PlaceTower();
            }
        }    
        else if (!EventSystem.current.IsPointerOverGameObject() && GameManager.Instance.ClickedBtn == null && Input.GetMouseButtonDown(0))
        {
            if (Input.GetMouseButtonDown(0) && myTower != null)           
            {
                GameManager.Instance.SelectTower(myTower);
            }
            if (Input.GetMouseButtonDown(0) && myTower == null)         
            {
                GameManager.Instance.DeselectTower();
            }
        }
    }

    public void PlaceTower()
    {
        GameObject tower = (GameObject)Instantiate(GameManager.Instance.ClickedBtn.TowerPrefab, transform.position, Quaternion.identity);

        tower.GetComponent<SpriteRenderer>().sortingOrder = (int)GridPosition.Y;

        tower.transform.SetParent(transform);

        this.myTower = tower.transform.GetChild(0).GetComponent<Tower>();

        Hover.Instance.Deactivate();

        IsEmpty = false;

        ColorTile(Color.white);

        myTower.Price = GameManager.Instance.ClickedBtn.Price;

        GameManager.Instance.BuyTower();

    }

    public void OnMouseExit()
    {
        ColorTile(Color.white);
    }

    public void ColorTile(Color newColor)
    {
        spriteRenderer.color = newColor;
    }
}
