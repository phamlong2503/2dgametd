﻿
class FrostDebuff : Debuff
{

    private float slowingFactor;

    private bool applied;

    public FrostDebuff(float slowingFactor, float duration, Monster target) : base(duration,target)
    {

        this.slowingFactor = slowingFactor;
    }


    public override void Update()
    {
        if (target != null) 
        {
            if (!applied) 
            {
                applied = true; 


                target.Speed -= (target.MaxSpeed*slowingFactor)/100;
            }

            base.Update();
        }

    }

    public override void Remove()
    {
        if (target != null)
        {
            target.Speed = target.MaxSpeed;

            base.Remove();
        }

    }
}