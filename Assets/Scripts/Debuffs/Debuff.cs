﻿using UnityEngine;


public abstract class Debuff
{

    protected float duration;


    protected float elapsed;


    protected Monster target;


    public Debuff(float duration, Monster target)
    {
        this.target = target;
        this.duration = duration;
    }


    public virtual void Update()
    {
        elapsed += Time.deltaTime;

        if (elapsed >= duration)
        {
            Remove();
        }
    }
    
    public virtual void Remove()
    {
        if (target != null)
        {
            target.DebuffsToRemove.Add(this);
        }
        
    }
}
