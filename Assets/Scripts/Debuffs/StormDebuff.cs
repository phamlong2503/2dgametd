﻿using System;


[Serializable]
public class StormDebuff : Debuff
{

    private float speed;

    public StormDebuff(Monster target, float duration) : base(duration,target)
    {
        if (target != null) 
        {
            this.speed = target.Speed;
            target.Speed = 0;
        }
     
    }


    public override void Remove()
    {
        if (target != null)
        {
            target.Speed = target.MaxSpeed;
            base.Remove();
        }

    }
}
