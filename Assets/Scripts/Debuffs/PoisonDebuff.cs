﻿using UnityEngine;

[System.Serializable]
class PoisonDebuff : Debuff
{

    private float tickTime;

    private float timeSinceTick;

    private PoisonSplash splashPrefab;

    private int splashDamage;

    public PoisonDebuff(int splashDamage, float tickTime, PoisonSplash splashPrefab, Monster target, float duration) : base(duration, target)
    {
        this.splashDamage = splashDamage;
        this.tickTime = tickTime;
        this.splashPrefab = splashPrefab;
    }

    public override void Update()
    {
        if (target != null) 
        {
            timeSinceTick += Time.deltaTime;

            if (timeSinceTick >= tickTime)
            {
                timeSinceTick = 0;
                Splash();
            }
            base.Update();
        }

    }

    private void Splash()
    {
        PoisonSplash tmp = GameObject.Instantiate(splashPrefab, target.transform.position, Quaternion.identity) as PoisonSplash;
        tmp.Damage = splashDamage;
        Physics2D.IgnoreCollision(target.GetComponent<Collider2D>(), tmp.GetComponent<Collider2D>());
    }
}
