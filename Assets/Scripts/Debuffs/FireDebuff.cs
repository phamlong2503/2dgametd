﻿using UnityEngine;


public class FireDebuff : Debuff
{

    private float tickTime;
    

    private float tickDamage;


    private float timeSinceTick;


    public FireDebuff( float tickDamage ,float tickTime, Monster target, float duration) : base(duration,target)
    {

        this.tickDamage = tickDamage;
        this.tickTime = tickTime;
    }


    public override void Update()
    {
        if (target != null) 
        {
            timeSinceTick += Time.deltaTime;

            if (timeSinceTick >= tickTime)
            {
                timeSinceTick = 0;
                target.TakeDamage(tickDamage, Element.FIRE);
            }
            base.Update();
        }

    }
}
