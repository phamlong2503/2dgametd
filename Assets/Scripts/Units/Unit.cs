﻿using System.Collections.Generic;
using UnityEngine;

abstract public class Unit : MonoBehaviour
{
    [SerializeField]
    private float speed;

    public Point GridPosition { get; set; }

    protected Animator myAnimator;

    private SpriteRenderer spriteRenderer;

    private Stack<Node> path;

    private Vector3 destination;

    public bool IsActive { get; set; }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            this.speed = value;
        }
    }

    protected virtual void Awake()
    {
        myAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void Update()
    {
        Move();
    }

    public void Animate(Point currentPos, Point newPos)
    {
        if (currentPos.Y > newPos.Y)
        {
            myAnimator.SetFloat("Horizontal", 0);

            myAnimator.SetFloat("Vertical", 1);
        }
        else if (currentPos.Y < newPos.Y)
        {
            myAnimator.SetFloat("Horizontal", 0);
            myAnimator.SetFloat("Vertical", -1);
        }
        if (currentPos.Y == newPos.Y)
        {
            if (currentPos.X > newPos.X)
            {
                myAnimator.SetFloat("Vertical", 0);
                myAnimator.SetFloat("Horizontal", -1);
            }
            else if (currentPos.X < newPos.X)
            {
                myAnimator.SetFloat("Vertical", 0);
                myAnimator.SetFloat("Horizontal", 1);
            }
        }
    }

    public void Move()
    {
        if (IsActive) 
        {
            transform.position = Vector2.MoveTowards(transform.position, destination, Speed * Time.deltaTime);
            if (transform.position == destination)
            { 
                if (path != null && path.Count > 0)
                {
                    Animate(GridPosition, path.Peek().GridPosition);
                    GridPosition = path.Peek().GridPosition;
                    destination = path.Pop().WorldPosition;

                }
                else 
                {
                    IsActive = false;
                }
            }
        }
    }

    public void SetPath(Stack<Node> newPath, bool active)
    {
        if (newPath != null) 
        {
            this.path = newPath;
            Animate(GridPosition, path.Peek().GridPosition);
            GridPosition = path.Peek().GridPosition;
            destination = path.Pop().WorldPosition;
            this.IsActive = active;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Tile") 
        {
            spriteRenderer.sortingOrder = (int)other.GetComponent<TileScript>().GridPosition.Y;
        }
    }

}