﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monster : Unit
{

    [SerializeField]
    private Stat monsterHealth;

    [SerializeField]
    private Element elementType;


    private List<Debuff> debuffs = new List<Debuff>();


    public List<Debuff> newDebuffs = new List<Debuff>();


    public List<Debuff> DebuffsToRemove { get; private set; }


    private int invulnerability = 2;

    public float MaxSpeed { get; private set; }


    public bool Alive
    {
        get
        {
            return monsterHealth.CurrentValue > 0;
        }
    }


    public Element ElementType
    {
        get
        {
            return elementType;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        DebuffsToRemove = new List<Debuff>();
        MaxSpeed = Speed;
        monsterHealth.Initialize();
    }

    protected override void Update()
    {
        HandleDebuffs();
        base.Update();
    }

    public void AddDebuff(Debuff debuff)
    {
        if (!debuffs.Exists(x => x.GetType() == debuff.GetType()))
        {
            newDebuffs.Add(debuff);
        }
    }


    private void HandleDebuffs()
    {
        if (newDebuffs.Count > 0)
        {
            debuffs.AddRange(newDebuffs);
            newDebuffs.Clear();
        }
        foreach (Debuff debuff in DebuffsToRemove)
        {
            debuffs.Remove(debuff);
        }
        DebuffsToRemove.Clear();
        foreach (Debuff debuff in debuffs) 
        {
            debuff.Update();
        }
    }

    public void TakeDamage(float damage, Element dmgSource)
    {
        if (IsActive)
        {
            if (dmgSource == elementType)
            {
                damage = damage / invulnerability;
                invulnerability++;
            }
            monsterHealth.CurrentValue -= damage;
            if (monsterHealth.CurrentValue <= 0)
            {
                SoundManager.Instance.PlaySFX("Splat");
                GameManager.Instance.Currency += 2;
                myAnimator.SetTrigger("Die");
                GetComponent<SpriteRenderer>().sortingOrder--;
                IsActive = false;       
            }
        }
    }
    public IEnumerator Scale(Vector3 from, Vector3 to, bool remove)
    {
        float progress = 0;
        while (progress <= 1)
        {
            transform.localScale = Vector3.Lerp(from, to, progress);
            progress += Time.deltaTime * 1;
            yield return null;
        }
        transform.localScale = to;
        IsActive = true;
        if (remove)
        {
            Release();
        }

    }

    public void Spawn(int health)
    {
        this.monsterHealth.MaxVal = health;
        this.monsterHealth.CurrentValue = health;
        debuffs.Clear();
        transform.position = LevelManager.Instance.BluePortal.transform.position;
        StartCoroutine(Scale(new Vector3(0.1f, 0.1f), new Vector3(1, 1), false));
        SetPath(LevelManager.Instance.Path, false);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        if (other.name == "RedPortal")
        {
            other.GetComponent<Portal>().Animate();
            StartCoroutine(Scale(new Vector3(1, 1), new Vector3(0.1f, 0.1f), true));
            GameManager.Instance.Lives--;
        }
    }

    public void Release()
    {
        foreach (Debuff debuff in debuffs)
        {
            debuff.Remove();
        }
        GridPosition = new Point(0, 0);
        Speed = MaxSpeed;
        GameManager.Instance.RemoveMonster(this);
        GameManager.Instance.Pool.ReleaseObject(gameObject);
    }
}
